package com.javaweb.employservice.service;

import com.javaweb.employservice.dto.CreateOffertRequest;
import com.javaweb.employservice.entity.OfferWork;
import com.javaweb.employservice.entity.Work;
import com.javaweb.employservice.repository.OffertWorkRepository;
import com.javaweb.employservice.repository.WorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Service
public class OffertWorkService {
    @Autowired
    OffertWorkRepository offertWorkRepository;
    @Autowired
    WorkRepository workRepository;
    public OfferWork crearOferta(OfferWork o){
        return offertWorkRepository.insert(o);
    }
    public List<OfferWork> getAll(){
        return offertWorkRepository.findAll();
    }
    public OfferWork getUserById(String id){
        return offertWorkRepository.findById(id).orElse(null);
    }
    public void borrarOferta(String id){
        offertWorkRepository.deleteById(id);
    }
/*    public OfferWork saveOferta(OfferWork o){
        OfferWork userNew=offertWorkRepository.save(o);
        return userNew;*/
    public OfferWork saveOferta(CreateOffertRequest request){
        OfferWork oferta = new OfferWork();
        oferta.setNombre(request.getNombre());
        oferta.setTrabajo(new HashSet<Work>());
        oferta.setDescripcionzona(request.getDescripcionzona());
        oferta.setDescripcion(request.getDescripcion());
        oferta.setFecha(request.getFecha());

        return offertWorkRepository.save(oferta);

    }
}
