package com.javaweb.employservice.controller;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaweb.employservice.dto.SignupRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@CrossOrigin(origins = "*",methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/user")
public class UserController {

    private RestTemplate restTemplate;

    @Autowired
    public UserController(RestTemplateBuilder Builder){
        this.restTemplate = Builder
                .messageConverters(createMappingJacksonHttpMessageConverter())
                .build();
    }

    @PostMapping("/register")
    //@ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody SignupRequest user){
        HttpHeaders headers= new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SignupRequest> entity= new HttpEntity<>(user,headers);
        ResponseEntity<String>responseEntity=restTemplate.exchange("http://localhost:8082/api/auth/signup",
                HttpMethod.POST,
                entity,String.class);
        HttpStatus statusCode=responseEntity.getStatusCode();
        System.out.println("status code ="+statusCode);
        String ase=responseEntity.getBody();
        System.out.println("respuesta body ="+ase);
        HttpHeaders responseHeaders=responseEntity.getHeaders();
        System.out.println("respuesta header ="+responseHeaders);

    }
    private ObjectMapper createObjectMapper() {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        return objectMapper;
    }
    private MappingJackson2HttpMessageConverter createMappingJacksonHttpMessageConverter() {

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(createObjectMapper());
        return converter;
    }
}
