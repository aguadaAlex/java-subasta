package com.javaweb.employservice.controller;

import com.javaweb.employservice.dto.CreateOffertRequest;
import com.javaweb.employservice.entity.OfferWork;
import com.javaweb.employservice.repository.OffertWorkRepository;
import com.javaweb.employservice.service.OffertWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@CrossOrigin(origins = "*",methods = {RequestMethod.POST,RequestMethod.GET,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/tasks")
public class OfferController {
    @Autowired
    private OffertWorkRepository offertWorkRepository;
    @Autowired
    private OffertWorkService offertWorkService;


    @PostMapping("/task")
    public OfferWork create(@Validated @RequestBody CreateOffertRequest request){
        return offertWorkService.saveOferta(request);
    }
    @GetMapping("/")
    public List<OfferWork> readAll(){
        return offertWorkRepository.findAll();
    }
    @PutMapping("/task/{id}")
    public OfferWork update(@PathVariable String id, @Validated @RequestBody OfferWork u){
        return offertWorkRepository.save(u);
    }
    @DeleteMapping("/task/{id}")
    public void delete(@PathVariable String id){
        offertWorkRepository.deleteById(id);
    }

}
