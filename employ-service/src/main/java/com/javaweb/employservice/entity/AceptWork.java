/*
package com.javaweb.employservice.entity;
import lombok.Getter;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
@Document(collection = "aceptaworks")
public class AceptWork {
    @Id
    @Getter
    @Setter
    private String id;
    @DBRef
    @Getter @Setter
    private OfferWork ofertaTrabajo;
    @DBRef
    @Getter @Setter
    private DemandWork solicitud;
    @Getter @Setter
    @NotBlank
    @Size(max = 320)
    private String descripcion;
}
*/
