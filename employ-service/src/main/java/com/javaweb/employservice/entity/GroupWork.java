/*
package com.javaweb.employservice.entity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Document(collection = "groupworks")
public class GroupWork {
    @Id
    @Getter
    @Setter
    private String id;
    @Getter @Setter
    @NotBlank
    @Size(max = 20)
    private String nombre;
    @DBRef
    @Getter @Setter
    private Set<Work> works;
    @Getter @Setter
    @NotBlank
    @Size(max = 320)
    private String descripcion;
}
*/
