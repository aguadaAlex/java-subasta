/*
package com.javaweb.employservice.entity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "demandworks")
public class DemandWork {
    @Id
    @Getter
    @Setter
    private String id;
    @Getter @Setter
    @NotBlank
    @Size(max = 10)
    private Integer usuario;
    @DBRef
    @Getter @Setter
    private OfferWork ofertaTrabajo;
    @Getter @Setter
    @NotBlank
    @Size(max = 10)
    private String precio;
    @Getter @Setter
    @NotBlank
    @Size(max = 320)
    private String descripcion;

}
*/
