package com.javaweb.employservice.repository;
import com.javaweb.employservice.entity.OfferWork;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface OffertWorkRepository extends MongoRepository<OfferWork, String>{

}
