/*package com.javaweb.employservice.repository;

import com.javaweb.employservice.entity.DemandWork;
import com.javaweb.employservice.entity.OfferWork;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DemandWorkRepository extends MongoRepository<DemandWork, String>{
    Optional<OfferWork> findByNombre(String nombre);
    Boolean exitsByNombre(String nombre);
}
*/
